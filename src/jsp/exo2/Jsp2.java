package jsp.exo2;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Jsp2
 */
@WebServlet("/Jsp2")
public class Jsp2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Jsp2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.isNew() || session.getAttribute("find").toString().matches("true")) {
			session.setAttribute("num", (new Random()).nextInt(100));
			session.setAttribute("find", false);
			session.setAttribute("eval", null);
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/game.jsp");
        dispatcher.forward(request, response); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer val;
		try{
			val = Integer.parseInt(request.getParameter("num"));
		}catch (Exception e) {
			throw new NumberFormatExeption(); 
		}
		HttpSession session = request.getSession();
		Integer res = Integer.parseInt(session.getAttribute("num").toString());
		if(res == val) {
			session.setAttribute("find", true);
			session.setAttribute("eval", null);
		}else if (res > val) {
			session.setAttribute("eval", true);
		}else {
			session.setAttribute("eval", false);
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/game.jsp");
        dispatcher.forward(request, response); 
	}

}
