package project.auth;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import project.model.Client;
import project.model.Utilisateur;

public class BoSecurity {

	private static final BoSecurity instance = new BoSecurity();
	private static Map<String,String> liste;
	private static Map<String,Utilisateur> listeUser;


	private BoSecurity() {
		liste = new HashMap<String, String>();
		listeUser = new HashMap<String, Utilisateur>();
		listeUser.put("red",new Utilisateur("red", "Laurent", "Martin", new Date()));
		listeUser.put("blue",new Utilisateur("blue", "Ketsia", "Dubois", new Date()));
		listeUser.put("green",new Utilisateur("green", "Badrane", "Dupont", new Date()));
		listeUser.put("black",new Utilisateur("black", "Amaia", "Bernard", new Date()));
		listeUser.put("white",new Utilisateur("white", "Linda", "Dupuis", new Date()));
		liste.put("red","123");
		liste.put("blue","456");
		liste.put("green","789");
		liste.put("black","147");
		liste.put("white","258");
	}

	public static BoSecurity getInstance() {
		return instance;

	}

	public Utilisateur authent(String username, String password) {

		if(liste.containsKey(username)) {
			if(liste.get(username).compareTo(password) == 0) {
				return listeUser.get(username);
			}
		}
		return null;
	}
	public Boolean addClient(String username,String nom,String prenom) {
		try {
			listeUser.get(username).getClients().put(Utilisateur.getId(), new Client("", nom, prenom));
			return true;
		}catch (Exception e) {
			return false;
		}

	}

	public Client getClient(String username, Integer cliId) {
		return listeUser.get(username).getClients().get(cliId);
	}

	public void deleteClient(String username, Integer cliId) {
		listeUser.get(username).getClients().remove(cliId);
	}
}
