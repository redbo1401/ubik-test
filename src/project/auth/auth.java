package project.auth;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import project.model.Utilisateur;

/**
 * Servlet implementation class auth
 */
@WebServlet("/auth")
public class Auth extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Auth() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		if(session.getAttribute("online") == null) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/project/auth.jsp");
			dispatcher.forward(request, response);
		}else {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/project/index.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		BoSecurity sec = BoSecurity.getInstance();
		Utilisateur user = sec.authent(username, password);
		if(sec.authent(username, password) != null) {
			HttpSession session = request.getSession();
			session.setAttribute("APP_PROFIL", user);
			session.setAttribute("online", true);	
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/project/index.jsp");
			dispatcher.forward(request, response);
		}else {
			HttpSession session = request.getSession();
			request.setAttribute("msg", "le nom d'utilisateur ou mot de passe est incorrect");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/project/auth.jsp");
			dispatcher.forward(request, response);
		}
	}

}
