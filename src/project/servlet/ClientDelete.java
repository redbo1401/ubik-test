package project.servlet;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import project.auth.BoSecurity;

/**
 * Servlet implementation class ClientDelete
 */
@WebServlet("/client-delete/*")
public class ClientDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Enumeration<String> en = request.getParameterNames();
		String username = request.getRequestURI().split("/")[3];
		BoSecurity bo = BoSecurity.getInstance();
		
		while (en.hasMoreElements()) {
			Integer cliId = Integer.parseInt((String) en.nextElement());
			bo.deleteClient(username, cliId);
		}
		response.sendRedirect("/exercice/clients");
	}

}
