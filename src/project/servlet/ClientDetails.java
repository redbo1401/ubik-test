package project.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import project.auth.BoSecurity;
import project.model.Client;

/**
 * Servlet implementation class ClientDetails
 */
@WebServlet("/client/*")
public class ClientDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer cliId =  Integer.parseInt(request.getRequestURI().split("/")[4]);
		String username = request.getRequestURI().split("/")[3];
		BoSecurity bo = BoSecurity.getInstance();
		Client cl = bo.getClient(username, cliId);
		request.setAttribute("client", cl);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/project/client.jsp");
        dispatcher.forward(request, response); 
	}


}
