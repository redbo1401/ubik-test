package project.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import project.auth.BoSecurity;

/**
 * Servlet implementation class ClientAdd
 */
@WebServlet("/client-add/*")
public class ClientAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/project/client-add.jsp");
        dispatcher.forward(request, response); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String user = request.getRequestURI().split("/")[3];
		BoSecurity bo = BoSecurity.getInstance();
		if(bo.addClient(user, nom, prenom)) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/project/firstpage.jsp");
	        dispatcher.forward(request, response); 
		}else {
			HttpSession session = request.getSession();
			session.setAttribute("msgAdd", "Le client n'a pas été ajouté");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/project/client-add.jsp");
	        dispatcher.forward(request, response); 
		}
	}

}
