package project.model;

public class Client {
	private String numeroClient;
	private String nom;
	private String prenom;

	public Client(String numeroClient, String nom, String prenom) {
		this.numeroClient = numeroClient;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public String getNumeroClient() {
		return numeroClient;
	}

	public void setNumeroClient(String numeroClient) {
		this.numeroClient = numeroClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Client [numeroClient=" + numeroClient + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
}
