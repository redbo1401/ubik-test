package project.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class Utilisateur {
	
	private String username;
	
	private String nom;
	private String prenom;
	private Date lastCo;
	private Date createDate;
	private Map<Integer,Client> clients;
	private static Integer id = new Integer(0);
	
	

	public Utilisateur(String username, String nom, String prenom, Date createDate) {
		super();
		this.username = username;
		this.nom = nom;
		this.prenom = prenom;
		this.createDate = createDate;
		this.clients = new HashMap<Integer, Client>();
		this.clients.put(getId(),new Client("3014","Diallo","Anthonie"));
		this.clients.put(getId(),new Client("3016","Parmentier","Nicola"));
		this.clients.put(getId(),new Client("3040","Bada","Valerie"));
		this.clients.put(getId(),new Client("3069","Sane","Soriba"));
		this.clients.put(getId(),new Client("3087","Lamiss","Anne"));
		
	}
	
	public Map<Integer,Client> getClients() {
		return clients;
	}
	public static Integer getId() {
		id++;
		return id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getLastCo() {
		return lastCo;
	}
	public void setLastCo(Date lastCo) {
		this.lastCo = lastCo;
	}
	public String getUsername() {
		return username;
	}
	public String getCreateDate() {
		SimpleDateFormat formater = new SimpleDateFormat("d MMM yyyy");
		
		return formater.format(createDate);
	}

}
