package taglib.exo1;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class DateTag extends TagSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String form = "dd/MM/yyyy hh:mm:ss";
	
	public DateTag() {
        super();
    }
 
    public int doStartTag() throws JspException {
        try{
        	Date date = new Date();
        	SimpleDateFormat formater = new SimpleDateFormat(form);
	    pageContext.getOut().write(formater.format(date));
	} catch(Exception e){
		
	}
        return EVAL_BODY_INCLUDE;
    }
    
    public int doEndTag() throws JspException 
    {
        return EVAL_PAGE;
    }

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

}
