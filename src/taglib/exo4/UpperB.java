package taglib.exo4;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class UpperB extends BodyTagSupport {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpperB() {
		super();
	}

	public int doStartTag() throws JspException {
		return EVAL_BODY_BUFFERED;
	}

	public int doAfterBody() throws JspException {
		BodyContent body = getBodyContent();
		try {
			if(body != null) {
				String res = body.getString().toUpperCase();
				body.getEnclosingWriter().println(res);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return EVAL_PAGE;
	}

}
