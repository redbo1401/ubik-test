package session.exo1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Exo1
 */
@WebServlet("/Session1")
public class Exo1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Exo1() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Servlet</title>");
		out.println("</head>");
		out.println("<body>");
		HttpSession session = request.getSession();
		if(!session.isNew()) {
			Integer res = Integer.parseInt(request.getParameter("num"));
			Integer val = Integer.parseInt(session.getAttribute("num").toString());
			if(res == val) {
				out.println("<p>Vous avez gagné le numéro est le " + val + "</p>");
			}else {
				if(res > val) {
					out.println("<p>Veuillez entrer un nombre entre 0 et 100 : </p>"
							+ "<form action=\"Session1\" method=\"post\"> <input type=\"text\" name=\"num\" id=\"\">");
					out.println("<button type=\"submit\">Envoyer</button></form></body>");
					out.println("<br/><p>c'est moins !" +"</p>");
				}else {
					out.println("<p>Veuillez entrer un nombre entre 0 et 100 : </p>"
							+ "<form action=\"Session1\" method=\"post\"> <input type=\"text\" name=\"num\" id=\"\">");
					out.println("<button type=\"submit\">Envoyer</button></form></body>");
					out.println("<br/><p>c'est plus !" +"</p>");
				}
			}
		}else {
			session.setAttribute("num", (new Random()).nextInt(100));
			out.println("<p>Veuillez entrer un nombre entre 0 et 100 : </p>"
					+ "<form action=\"Session1\" method=\"post\"> <input type=\"text\" name=\"num\" id=\"\">");
			out.println("<button type=\"submit\">Envoyer</button></form></body>");
		}
		
		out.println("</html>");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
