<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    <%@ page isErrorPage="false" errorPage="/WEB-INF/views/errorHandler.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Devine le nombre</title>
</head>
<body>

<c:if test="${!sessionScope.find}">
<p>Veuillez entrer un nombre entre 0 et 100</p><br/>
<form action="Jsp2" method="post">
<input type="text" name="num" >
<button type="submit">Envoyer</button>
</form>
</c:if>

<c:if test="${sessionScope.eval}">
<p>c'est plus</p>
</c:if>

<c:if test="${!sessionScope.eval  && sessionScope.eval != null}">
<p>c'est moins</p>
</c:if>
<c:if test="${sessionScope.find}">
<p>Bravo ! Vous avez gagn�, le num�ro est ${sessionScope.num}</p>
</c:if>


</body>
</html>