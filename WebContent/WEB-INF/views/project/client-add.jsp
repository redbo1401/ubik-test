<%@ include file="index.jsp"%>

<form action="client-add/${sessionScope.APP_PROFIL.username}" method="post">
  <div class="form-group">
    <label>Nom</label>
    <input type="text" name="nom" class="form-control" placeholder="nom">
  </div>
   <div class="form-group">
    <label>Prenom</label>
    <input type="text" name="prenom" class="form-control" placeholder="prenom">
  </div> 
  <button type="submit" class="btn btn-outline-info">Ajouter</button>
  <a href="/exercice/clients" class="btn btn-outline-info" >Annuler</a>
</form>

<c:out value="${sessionScope.msgAdd}"></c:out>
</div>
<%@ include file="footer.jsp"%>