<%@ include file="index.jsp"%>
<div class="col-8">
	<form onsubmit="return valide();" method="post" action="/exercice/client-delete/${sessionScope.APP_PROFIL.username}">
		<c:forEach var="client" items="${sessionScope.APP_PROFIL.clients }">

			<div>
				<input type="checkbox" name="${client.key}"> 
					<label> 
						<a href="/exercice/client/${sessionScope.APP_PROFIL.username}/${client.key}">
							<c:out value="${client.value.nom}" /> <c:out value="${client.value.prenom}" />
						</a>
				</label>
			</div>

		</c:forEach>

		<button class="btn btn-outline-danger" type="submit">Supprimer</button>
		<a href="client-add" class="btn btn-outline-info">Ajouter un
			client</a>
	</form>
	
</div>
</div>

<script type="text/javascript">
function valide(){
	return confirm('Voulez vous vraiment supprimer ces clients ?');
}
</script>

<%@ include file="footer.jsp"%>