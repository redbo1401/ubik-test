<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
<div class="col-10">
	<p>Bonjour, <c:out value="${sessionScope.APP_PROFIL.nom }"/>
	<c:out value="${sessionScope.APP_PROFIL.prenom }"/>
	, vous etes membre depuis le 
	<c:out value="${sessionScope.APP_PROFIL.createDate }"/>
</p>
</div>
<div class="col-3">
<a href="/exercice/logout" class="btn btn-outline-info">Deconnexion</a>
</div>
</nav>