<%@page import="javax.sound.midi.SysexMessage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<label>nombre de colonne : </label>
	<c:out value='${requestScope.abs}' />
	<br />
	<label>nombre de ligne : </label>
	<c:out value='${requestScope.ord}' />
	<table>
		<%
			int abs = Integer.parseInt(request.getAttribute("abs").toString());
			int ord = Integer.parseInt(request.getAttribute("ord").toString());

			for (int i = 1; i < ord + 1; i++) {
		%>
		<tr>
			<%
				for (int j = 1; j < abs + 1; j++) {
			%>
			<td>[<%=j%>;<%=i%>]
			</td>
			<%
				}
			%>
		</tr>
		<% } %>
	</table>
</body>
</html>